# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class MturfclubownerItem(scrapy.Item):
    # define the fields for your item here like:
    owner = scrapy.Field()
    ResultRaceTime = scrapy.Field()
    pass

class MturfclubItem(scrapy.Item):
    # define the fields for your item here like:
    Rrace = scrapy.Field()
    Rracedistance = scrapy.Field()
    Rracerating = scrapy.Field()
    Rracetime = scrapy.Field()

    HorseName = scrapy.Field()
    HorseNo = scrapy.Field()
    Stable = scrapy.Field()
    Perf = scrapy.Field()
    Age = scrapy.Field()
    Gear = scrapy.Field()
    Jockey = scrapy.Field()
    Draw = scrapy.Field()
    TimeFactor = scrapy.Field()


    RaceDate = scrapy.Field()

    owner = scrapy.Field()

    Result = scrapy.Field()
    RaceTime = scrapy.Field()
    NonRunners = scrapy.Field()
    Runners = scrapy.Field()
    Sectionals = scrapy.Field()

    url = scrapy.Field()
    pass

