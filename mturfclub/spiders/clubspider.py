# -*- coding: utf-8 -*-
import scrapy
from scrapy.loader import ItemLoader
from mturfclub.items import MturfclubItem, MturfclubownerItem



class ClubspiderSpider(scrapy.Spider):
    name = 'clubspider'
    allowed_domains = ['mauritiusturfclub.com']
    start_urls = []
    #start_urls = ['http://www.mauritiusturfclub.com/index.php/en/racing/racecards?meeting=20-2017']


    def __init__(self, meeting, *args, **kwargs):
        super(ClubspiderSpider, self).__init__(*args, **kwargs)
        self.start_urls = ['http://www.mauritiusturfclub.com/index.php/en/racing/racecards?meeting=%s' % (meeting)]
        self.meeting = meeting

    def parse(self, response):
        table = response.xpath('//table[@class="tblgrey"]')
        url1 = 'http://www.mauritiusturfclub.com/index.php/en/racing/racecards?view=owners&meeting=%s' % self.meeting
        request = scrapy.Request(url1,
                                 callback=self.parse_page2)
        request.meta['item'] = table
        yield request

    def parse_page2(self, response):
        url2 = 'http://www.mauritiusturfclub.com/index.php/en/racing/results?meeting=%s' % self.meeting
        request = scrapy.Request(
            url2, callback=self.parse_page3)

        table_owner = response.xpath('//table[@class="tblgrey"]')
        request.meta['item2'] = table_owner
        request.meta['item'] = response.meta['item']
        yield request

    def parse_page3(self, response):

        table_check = response.meta['item']
        nocontent = table_check.xpath('//div[@class="nocontent"]//text()').extract()

        if nocontent:
            self.logger.info('$$$$$$$')
            self.logger.info('$$$$$$$ %s' % nocontent[0])
            pass

            # # first table
            table_check
            table_body_check = table_check.xpath('./tbody')

            #heading from site1
            heading = table_check.xpath('./thead//tr/th[@class="aln_left"]/text()')
            heading_m = table_check.xpath('./thead//tr/th[@class="aln_left"]/following-sibling::*[1]/text()')
            heading_rating = table_check.xpath('./thead//tr/th[@class="aln_left"]/following-sibling::*[2]/text()')
            heading_time = table_check.xpath('./thead//tr/th[@class="aln_left"]/following-sibling::*[3]/text()')

            #third table (results)
            table_results = response.xpath('//ol/li/div[@class="results-holder"]/table[@class="result"]')
            table_body_results = table_results.xpath('./tbody')

            # loops
            for tb_results, head, head_m, head_r, head_t in zip(table_body_results, heading, heading_m, heading_rating, heading_time):

                # head
                head = head.extract()
                head_m = head_m.extract()
                head_r = head_r.extract()
                head_t = head_t.extract()

                # result
                tbs_results = tb_results.xpath('./tr[1]/td/text()').extract()
                tbs_results_time = tb_results.xpath('./tr[2]/td//b//text()').extract()
                tbs_results_NonRunners = tb_results.xpath('./tr[3]/td//b//text()').extract()
                tbs_results_Runners = tb_results.xpath('./tr[4]/td//b//text()').extract()
                tbs_results_Sectionals = tb_results.xpath('./tr[5]/td//b//text()').extract()

                l = ItemLoader(MturfclubItem(), response=response)
                # HEAD RaceDate,Race,Distance,Time,Rating
                l._add_value('Rrace', head)
                l._add_value('Rracedistance', head_m)
                l._add_value('Rracerating', head_r)
                l._add_value('Rracetime', head_t)

                # racedate
                l.add_xpath('RaceDate', '//option[@selected]/text()')

                # third table_results(Result,RaceTime, NonRunners,Runners,Sectionals)
                l.add_value('Result', tbs_results)
                l.add_value('RaceTime', tbs_results_time)
                l.add_value('NonRunners', tbs_results_NonRunners)
                l.add_value('Runners', tbs_results_Runners)
                l.add_value('Sectionals', tbs_results_Sectionals)

                l.add_value('url', self.start_urls)
                yield l.load_item()

        else:
            #third table (results)
            table_results = response.xpath('//ol/li/div[@class="results-holder"]/table[@class="result"]')
            table_body_results = table_results.xpath('./tbody')

            #second table (owners)
            table_owner = response.meta['item2']
            table_body_owner = table_owner.xpath('./tbody')

            #first table
            table = response.meta['item']
            table_body = table.xpath('./tbody')

            #heading from site1
            heading = table.xpath('./thead//tr/th[@class="aln_left"]/text()')
            heading_m = table.xpath('./thead//tr/th[@class="aln_left"]/following-sibling::*[1]/text()')
            heading_rating = table.xpath('./thead//tr/th[@class="aln_left"]/following-sibling::*[2]/text()')
            heading_time = table.xpath('./thead//tr/th[@class="aln_left"]/following-sibling::*[3]/text()')

            #loops
            for tb_owner,tb, tb_results, head, head_m, head_r, head_t in zip(table_body_owner, table_body,table_body_results, heading,
                                                             heading_m, heading_rating, heading_time):

                trs = tb.xpath('./tr')
                trs_owner = tb_owner.xpath('./tr')

                #head
                head = head.extract()
                head_m = head_m.extract()
                head_r = head_r.extract()
                head_t = head_t.extract()

                #result
                tbs_results = tb_results.xpath('./tr[1]/td/text()').extract()
                tbs_results_time = tb_results.xpath('./tr[2]/td//b//text()').extract()
                tbs_results_NonRunners = tb_results.xpath('./tr[3]/td//b//text()').extract()
                tbs_results_Runners = tb_results.xpath('./tr[4]/td//b//text()').extract()
                tbs_results_Sectionals = tb_results.xpath('./tr[5]/td//b//text()').extract()

                for tr, tr_owner in zip(trs,trs_owner):
                    #owner
                    item = MturfclubownerItem()
                    item['owner'] = tr_owner.xpath('./td[3]//text()').extract()
                    item_owner = item['owner']

                    #first and all
                    l = ItemLoader(MturfclubItem(), selector=tr)
                    #HEAD RaceDate,Race,Distance,Time,Rating
                    l._add_value('Rrace', head)
                    l._add_value('Rracedistance', head_m)
                    l._add_value('Rracerating', head_r)
                    l._add_value('Rracetime', head_t)
                    # l.add_value('time', '//table[@class="tblgrey"]/thead/thead[1]/tr[1]/th[4]/text()')
                    # l.add_value('rating', '//table[@class="tblgrey"]/thead/thead[1]/tr[1]/th[3]/text()')
                    #race
                    l.add_xpath('HorseName', './td[2]//text()')
                    l.add_xpath('HorseNo', './td[1]/text()')
                    l.add_xpath('Stable', './td[3]/text()')
                    l.add_xpath('Perf', './td[4]/text()')
                    l.add_xpath('Age', './td[5]/text()')
                    l.add_xpath('Gear', './td[6]/text()')
                    l.add_xpath('Jockey', './td[8]/text()')
                    l.add_xpath('Draw', './td[9]/text()')
                    l.add_xpath('TimeFactor', './td[10]/text()')

                    #racedate
                    l.add_xpath('RaceDate', '//option[@selected]/text()')
                    #owner()
                    l.add_value('owner', item_owner)
                    #third table_results(Result,RaceTime, NonRunners,Runners,Sectionals)
                    l.add_value('Result', tbs_results)
                    l.add_value('RaceTime', tbs_results_time)
                    l.add_value('NonRunners', tbs_results_NonRunners)
                    l.add_value('Runners', tbs_results_Runners)
                    l.add_value('Sectionals', tbs_results_Sectionals)

                    l.add_value('url', self.start_urls)
                    yield l.load_item()